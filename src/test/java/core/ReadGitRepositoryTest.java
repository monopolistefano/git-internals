package core;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import java.lang.String;


public class ReadGitRepositoryTest {

	private  String repoPath;
	private  GitRepository repository;
	
	@Before
	public void setup() {
		repoPath = new String("sample_repos/sample01");
		repository = new GitRepository(repoPath);
	}
	
	@Test
	public void shouldFindHead() throws Exception {
		assertThat(repository.getHeadRef()).isEqualTo("refs/heads/master");
	}
	
}
