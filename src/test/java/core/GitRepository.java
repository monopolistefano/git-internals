package core;

import java.lang.String;
import java.io.*;

public class GitRepository {

	private String repositoryPath;
	
	GitRepository (String repoPath){
		this.repositoryPath = repoPath;
		
	}
	
	public String getHeadRef()  {
		
		BufferedReader br = null;
		FileReader fr = null;
		
		String sCurrentLine = null;
		
		try {
		fr = new FileReader(repositoryPath+File.separator+"HEAD");
		br = new BufferedReader(fr);
		
		
		sCurrentLine = br.readLine();
		
		sCurrentLine = sCurrentLine.replace("ref: ", "");
		
		System.out.println(sCurrentLine);
		
		}
		catch (IOException e) 
		{
			
		}
		return sCurrentLine;
	}
	
	

}
